\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{Iterative Algorithms}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Steepest Descent}{7}{0}{2}
\beamer@subsectionintoc {2}{1}{Description}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{Convergence}{12}{0}{2}
\beamer@sectionintoc {3}{Conjugate Directions}{17}{0}{3}
\beamer@subsectionintoc {3}{1}{Conjugacy}{17}{0}{3}
\beamer@subsectionintoc {3}{2}{Gram-Schmidt}{22}{0}{3}
\beamer@sectionintoc {4}{Conjugate Gradients}{24}{0}{4}
\beamer@subsectionintoc {4}{1}{Impl. details}{27}{0}{4}
\beamer@sectionintoc {5}{Simulations}{29}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{31}{0}{6}
